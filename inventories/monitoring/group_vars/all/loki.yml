---

loki_version: "2.6.1"

#default redis config
cache_config_defaults: &CACHE_DEFAULTS
  background:
    writeback_goroutines: 20
  redis:
    endpoint: "127.0.0.1:6379"
  fifocache:
    max_size_bytes: 128MB
    max_size_items: 100
    ttl: 1h0m0s
    purgeinterval: 10s

loki_storage_dir: /opt/loki

loki_config_server:
  http_listen_address: "127.0.0.1"
  http_listen_port: 3100
  log_level: info
  http_server_write_timeout: 10m
  http_server_read_timeout: 10m
  http_server_idle_timeout: 10m
  grpc_server_max_send_msg_size: 8388608
  grpc_server_max_recv_msg_size: 8388608
  graceful_shutdown_timeout: 2m

loki_config_distributor:
  ring:
    kvstore:
      store: inmemory
    heartbeat_timeout: 30s

loki_config_querier:
  engine:
    timeout: 5m
    max_look_back_period: 30s
  query_timeout: 30m
  query_ingesters_within: 10m
  max_concurrent: 20

loki_config_query_scheduler:
  # instance_id: "{{ ansible_hostname }}"
  # use_scheduler_ring: true
  scheduler_ring:
    heartbeat_timeout: 10s
    kvstore:
      store: inmemory

loki_config_ingester_client:
  pool_config:
    client_cleanup_period: 30s
    health_check_ingesters: true

loki_config_ingester:
  chunk_encoding: snappy
  chunk_idle_period: 30m
  chunk_block_size: 2621440
  chunk_retain_period: 1m
  max_chunk_age: 120m
  max_transfer_retries: 0
  lifecycler:
    ring:
      kvstore:
        store: inmemory
      replication_factor: 1
    #interface_names:
    #  - ens192
    final_sleep: 10s
  wal:
    enabled: true
    flush_on_shutdown: true

loki_config_storage:
  index_cache_validity: 10m0s
  disable_broad_index_queries: true
  index_queries_cache_config:
    <<: *CACHE_DEFAULTS

loki_config_chunk_store:
  chunk_cache_config:
    <<: *CACHE_DEFAULTS

loki_config_query_range:
  align_queries_with_step: true
  parallelise_shardable_queries: true
  max_retries: 5
  cache_results: true
  results_cache:
    cache:
      <<: *CACHE_DEFAULTS

loki_config_frontend:
  log_queries_longer_than: 10s
  compress_responses: true
  max_outstanding_per_tenant: 2048

loki_config_frontend_worker:
  parallelism: 4
  grpc_client_config:
    # 16GiB
    max_send_msg_size: 17179869184
    max_recv_msg_size: 17179869184

loki_config_limits:
  enforce_metric_name: false
  ingestion_burst_size_mb: 64
  ingestion_rate_mb: 16
  max_global_streams_per_user: 10000
  max_query_parallelism: 64
  max_streams_per_user: 10000
  reject_old_samples: true
  reject_old_samples_max_age: 168h

...
