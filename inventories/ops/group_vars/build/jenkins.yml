---

jenkins_version: 2.462 # latest

jenkins_bind_address: 127.0.0.1

jenkins_admin:
  username: admin
  password: admin
  email: jenkins@cm.local
  frontend_url: "http://jenkins.cm.local"
  system_message: "Welcome to the Jenkins for CoreMedia"

jenkins_ldap:
  # LDAP server name, optionally with TCP port number, like "ldap.acme.org"
  # or "ldap.acme.org:389".
  ldap_server: ldap://glauth:389
  # The root DN to connect to. Normally something like "dc=sun,dc=com"
  root_dn: "dc=cm,dc=local"
  # if non-null, we use this and {@link #managerPassword}
  # when binding to LDAP.
  #
  # This is necessary when LDAP doesn't support anonymous access.
  manager:
    dn: "cn=admin,ou=admins,dc=cm,dc=local"
    # Scrambled password, used to first bind to LDAP.
    password: "{{ vault__ldap.admin }}"
  user:
    search_base: "ou=users"
    search_filter: "uid={0}"
  group:
    search_base: "ou=groups"
    search_filter: ""
  display:
    name_attribute_name: "cn"
    email_attribute_name: "mail"
  mail:
    disable_address_resolver: true
  environment_properties:
    'com.sun.jndi.ldap.connect.timeout': '5000'
    'com.sun.jndi.ldap.read.timeout': '6000'

jenkins_dsl_init:
  - pipeline: seed
    description: "read pipelines definitions from ...."
    enabled: true
    scm:
      remote: "https://git.zweit-hirn.de/coremedia/jenkins-pipelines.git"
      credentials: "users.git.coremedia"
      branch: "main"
    triggers:
      cron: "H 8-17/1 * * * 1-5"
      scm: "H 8-17/1 * * 1-5"

jenkins_init_groovy_scripts:
  - name: setup-jenkins.groovy
  - name: setup-admin.groovy
    priority: "002"
  - name: setup-authorization-strategy.groovy
    priority: "003"
  - name: disable-usage-stats.groovy
    priority: "004"
  - name: credentials-sshkeys.groovy
    priority: "005"
  - name: credentials-users.groovy
    priority: "005"
  - name: credentials-aws.groovy
    priority: "005"
    state: absent
  - name: credentials-files.groovy
    priority: "005"
    state: absent
  - name: create-seed-job.groovy
    priority: 090
  - name: approve-pending-signatures.groovy
    priority: 095
  - name: remove-credential-files.groovy
    priority: 099

jenkins_custom_env:
  PIPELINE_JENKINS_REPOSITORY: "https://git.zweit-hirn.de/coremedia/jenkins-pipelines.git"
  PIPELINE_JENKINS_CREDENTIALS_ID: "users.git.coremedia"
  # repository who located coremedia code
  PIPELINE_COREMEDIA_REPOSITORY: "https://git.zweit-hirn.de/coremedia/cmcc-11.git"
  PIPELINE_COREMEDIA_CREDENTIALS_ID: users.git.coremedia
  # repository to build container
  PIPELINE_CONTAINER_REPOSITORY: "https://git.zweit-hirn.de/coremedia"
  PIPELINE_CONTAINER_CREDENTIALS_ID: users.git.coremedia
  # container registry
  CONTAINER_REGISTRY: "registry.cm.local"
  CONTAINER_REGISTRY_CREDENTIALS_ID: "registry.jenkins"
  CONTAINER_REGISTRY_TYPE: "registry"
  # repository to ansible deployment
  PIPELINE_DEPLOYMENT_REPOSITORY: https://git.zweit-hirn.de/coremedia/deployment.git
  PIPELINE_DEPLOYMENT_CREDENTIALS_ID: "users.git.coremedia"

jenkins_secrets:
  import: true
  delete_after_import: false
  users:
    - user: bodsch
      password: aGFzdC1kdS10b2xsLWdlbWFjaHQhCg==
    - id: user.jenkins
      user: "jenkins"
      password: "jenkins"

jenkins_plugin_management:
  enabled: true
  version: 2.12.8

jenkins_plugin_updates:
  enabled: true

jenkins_plugins:
  - name: adoptopenjdk
  - name: ansicolor
  - name: authentication-tokens
  - name: bouncycastle-api
  - name: command-launcher
  - name: configuration-as-code
  - name: cloudbees-disk-usage-simple
  - name: credentials-binding
  - name: credentials
  - name: dashboard-view
  - name: envinject-api
  - name: envinject
  - name: git-client
  - name: git-server
  - name: git
  - name: groovy
  - name: jaxb
  - name: job-dsl
  - name: matrix-auth
  - name: metrics
  - name: parameterized-scheduler
  - name: pipeline-build-step
  - name: pipeline-graph-analysis
  - name: pipeline-input-step
  - name: pipeline-milestone-step
  - name: pipeline-model-api
  - name: pipeline-model-definition
  - name: pipeline-model-extensions
  - name: pipeline-stage-step
  - name: pipeline-stage-tags-metadata
  - name: plain-credentials
  - name: prometheus
  - name: scm-api
  - name: script-security
  - name: snakeyaml-api
  - name: throttle-concurrents
  - name: update-sites-manager
    version: 2.0.0
  - name: workflow-api
  - name: workflow-basic-steps
  - name: workflow-cps
  - name: workflow-durable-task-step
  - name: workflow-job
  - name: workflow-multibranch
  - name: workflow-scm-step
  - name: workflow-step-api
  - name: workflow-support
  - name: view-job-filters
  - name: rich-text-publisher-plugin
    state: absent
  - name: console-column-plugin
  - name: docker-plugin

...
