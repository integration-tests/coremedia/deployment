---

coremedia_hosts_domain: cm.local

management_container:
  name: "registry.cm.local/coremedia/management-tools:dev"

coremedia_directory:
  licenses: /opt/coremedia/licenses
  events_sitemap: /opt/coremedia/events-sitemap
  env: "{{ container_env_directory }}"
  cache: "{{ container_env_directory }}/cache"
  heapdumps: "{{ container_env_directory }}/heapdumps"
  blobcache: "{{ container_env_directory }}/blob_cache"
  tmp: "{{ container_env_directory }}/tmp"

coremedia_urls:
  studio: "https://studio.{{ snakeoil_domain }}"
  preview: "https://preview.{{ snakeoil_domain }}"
  live: "https://www.{{ snakeoil_domain }}"

coremedia_dba_user:
  cms: "{{ vault__database.content_management_server }}"
  mls: "{{ vault__database.master_live_server }}"
  rls1: "{{ vault__database.replication_live_server }}"
  rls2: "{{ vault__database.replication_live_server }}"
  caefeeder_preview: "{{ vault__database.caefeeder_preview }}"
  caefeeder_live: "{{ vault__database.caefeeder_live }}"
  content_feeder: "{{ vault__database.content_feeder }}"
  studio_editorial: "{{ vault__database.studio_editorial }}"

coremedia_ior:
  cms: "{{ 'cms' | bodsch.coremedia.ior_url(hostname='content-management-server', domainname=coremedia_hosts_domain, port='40180') }}"    # http://content-management-server.{{ coremedia_hosts_domain }}:40180/ior"
  mls: "{{ 'mls' | bodsch.coremedia.ior_url(hostname='master-live-server', domainname=coremedia_hosts_domain, port='40280') }}"           # http://master-live-server.{{ coremedia_hosts_domain }}:40280/ior"
  wfs: "{{ 'wfs' | bodsch.coremedia.ior_url(hostname='workflow-server', domainname=coremedia_hosts_domain, port='40380') }}"              # http://workflow-server.{{ coremedia_hosts_domain }}:40380/ior"
  rls1: "{{ 'rls' | bodsch.coremedia.ior_url(hostname='replication-live-server-01', domainname=coremedia_hosts_domain, port='42080') }}"  # http://replication-live-server-01.{{ coremedia_hosts_domain }}:42080/ior"
  rls2: "{{ 'rls' | bodsch.coremedia.ior_url(hostname='replication-live-server-02', domainname=coremedia_hosts_domain, port='42080') }}"  # http://replication-live-server-02.{{ coremedia_hosts_domain }}:42080/ior"

coremedia_solr:
  primary_cms: "http://solr-primary-cms.{{ coremedia_hosts_domain }}:40080/solr"
  primary_mls: "http://solr-primary-mls.{{ coremedia_hosts_domain }}:40080/solr"
  #  No solr replica in stage available
  replica1: "http://solr-replica-01.{{ coremedia_hosts_domain }}:40080/solr"
  replica2: "http://solr-replica-02.{{ coremedia_hosts_domain }}:40080/solr"

coremedia_container_properties:
  rls:
    - 'replicator.properties'
    - 'logging.properties'
    - 'sql.properties'
  cae_live:
    - 'logging.properties'
    - cache.properties
  cae_preview:
    - 'logging.properties'

coremedia_dba_endpoint:
  cms: dba-backend.cm.local
  mls: dba-frontend.cm.local
  rls1: dba-frontend.cm.local
  rls2: dba-frontend.cm.local
  studio_editorial: dba-backend.cm.local
  cae_feeder_live: dba-frontend.cm.local
  cae_feeder_preview: dba-backend.cm.local
  content_feeder: dba-backend.cm.local

coremedia_sql_dialect:
  postgres: "{{ 'postgres' | bodsch.coremedia.sql_dialect() }}"         # org.hibernate.dialect.PostgreSQLDialect
  mssql: "{{ 'mssql' | bodsch.coremedia.sql_dialect() }}"               # org.hibernate.dialect.SQLServer2008Dialect

# coremedia_sql_driver_defaults:
#   mssql: com.microsoft.sqlserver.jdbc.SQLServerDriver
#   postgres: org.postgresql.Driver
#   mysql: com.mysql.cj.jdbc.Driver

coremedia_sql_driver:
  cms: "{{ 'mysql' | bodsch.coremedia.sql_driver() }}"                  #  coremedia_sql_driver_defaults.mysql }}"
  mls: "{{ 'mysql' | bodsch.coremedia.sql_driver() }}"                  #   coremedia_sql_driver_defaults.mysql }}"
  rls1: "{{ 'mysql' | bodsch.coremedia.sql_driver() }}"                 #   coremedia_sql_driver_defaults.mysql }}"
  rls2: "{{ 'mysql' | bodsch.coremedia.sql_driver() }}"                 #   coremedia_sql_driver_defaults.mysql }}"
  cae_feeder_preview: "{{ 'mysql' | bodsch.coremedia.sql_driver() }}"   #   coremedia_sql_driver_defaults.mysql }}"
  cae_feeder_live: "{{ 'mysql' | bodsch.coremedia.sql_driver() }}"      #   coremedia_sql_driver_defaults.mysql }}"
  content_feeder: "{{ 'mysql' | bodsch.coremedia.sql_driver() }}"       #   coremedia_sql_driver_defaults.mysql }}"
  studio_editorial: "{{ 'mysql' | bodsch.coremedia.sql_driver() }}"     #   coremedia_sql_driver_defaults.mysql }}"

# coremedia_jdbc:
#   mssql: "jdbc:sqlserver"
#   postgres: "jdbc:postgresql"
#   mysql: "jdbc:mysql"

coremedia_dba:
  cms: "{{ 'mysql' | bodsch.coremedia.dba_url(db_endpoint=coremedia_dba_endpoint.cms, db_schema=coremedia_dba_user.cms) }}"                                               #  coremedia_jdbc.mysql }}://{{ coremedia_dba_endpoint.cms }}:3306/{{ coremedia_dba_user.cms }}"
  mls: "{{ 'mysql' | bodsch.coremedia.dba_url(db_endpoint=coremedia_dba_endpoint.mls, db_schema=coremedia_dba_user.mls) }}"                                               #  "{{ coremedia_jdbc.mysql }}://{{ coremedia_dba_endpoint.mls }}:3306/{{ coremedia_dba_user.mls }}"
  rls1: "{{ 'mysql' | bodsch.coremedia.dba_url(db_endpoint=coremedia_dba_endpoint.rls1, db_schema=coremedia_dba_user.rls1) }}"                                            #  "{{ coremedia_jdbc.mysql }}://{{ coremedia_dba_endpoint.rls1 }}:3306/{{ coremedia_dba_user.rls1 }}"
  rls2: "{{ 'mysql' | bodsch.coremedia.dba_url(db_endpoint=coremedia_dba_endpoint.rls2, db_schema=coremedia_dba_user.rls2) }}"                                            #   "{{ coremedia_jdbc.mysql }}://{{ coremedia_dba_endpoint.rls2 }}:3306/{{ coremedia_dba_user.rls2 }}"
  cae_feeder_preview: "{{ 'mysql' | bodsch.coremedia.dba_url(db_endpoint=coremedia_dba_endpoint.cae_feeder_preview, db_schema=coremedia_dba_user.caefeeder_preview) }}"  #   "{{ coremedia_jdbc.mysql }}://{{ coremedia_dba_endpoint.cae_feeder_preview }}:3306/{{ coremedia_dba_user.caefeeder_preview }}"
  cae_feeder_live: "{{ 'mysql' | bodsch.coremedia.dba_url(db_endpoint=coremedia_dba_endpoint.cae_feeder_live, db_schema=coremedia_dba_user.caefeeder_live) }}"           #   "{{ coremedia_jdbc.mysql }}://{{ coremedia_dba_endpoint.cae_feeder_live }}:3306/{{ coremedia_dba_user.caefeeder_live }}"
  content_feeder: "{{ 'mysql' | bodsch.coremedia.dba_url(db_endpoint=coremedia_dba_endpoint.content_feeder, db_schema=coremedia_dba_user.content_feeder) }}"              #   "{{ coremedia_jdbc.mysql }}://{{ coremedia_dba_endpoint.content_feeder }}:3306/{{ coremedia_dba_user.content_feeder }}"
  studio_editorial: "{{ 'mysql' | bodsch.coremedia.dba_url(db_endpoint=coremedia_dba_endpoint.studio_editorial, db_schema=coremedia_dba_user.studio_editorial) }}"        #   "{{ coremedia_jdbc.mysql }}://{{ coremedia_dba_endpoint.studio_editorial }}:3306/{{ coremedia_dba_user.studio_editorial }}"

coremedia_mongodb:
  cms:
    url: "mongodb://coremedia:{{ vault__mongodb.coremedia }}@mongodb.cms.{{ coremedia_hosts_domain }}:27017/coremedia?authSource=admin"
  mls:
    url: "mongodb://coremedia:{{ vault__mongodb.coremedia }}@mongodb.mls.{{ coremedia_hosts_domain }}:27017/coremedia?authSource=admin"

...
