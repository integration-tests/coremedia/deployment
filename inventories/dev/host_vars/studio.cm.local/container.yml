---

container_defaults: &CONTAINER_DEFAULTS
  restart_policy: always
  networks:
    - name: coremedia
  log_driver: "{{ container_log.log_driver }}"
  log_options: "{{ container_log.log_options }}"

container_labels: &CONTAINER_DEFAULT_LABELS
  labels:
    environment: dev
    service-discover: "true"

default_healthcheck: &CONTAINER_DEFAULT_HEALTH
  healthcheck:
    test:
      - "CMD"
      - "curl"
      - "-Lf"
      - "http://localhost:8081/actuator/health"
    interval: 30s
    timeout: 10s
    retries: 3
    start_period: 40s

container_studio:
  #
  - name: studio-server
    <<: *CONTAINER_DEFAULTS
    <<: *CONTAINER_DEFAULT_LABELS
    <<: *CONTAINER_DEFAULT_HEALTH
    image: "{{ container_registry_dev }}/coremedia/studio-server:{{ container_tags.coremedia }}"
    cpu: 1
    memory: 1537m
    memory_swap: 1537m
    memory_reservation: 1537m
    published_ports:
      - 41080:8080
      - 41081:8081
      - 41099:8199  # prometheus_exporter
    mounts: "{{ 'studio-server' | bodsch.coremedia.container_mounts(
            coremedia_directory,
            need_mounts=['prometheus', 'cache', 'state']
        ) }}"
    # mounts:
    #   - source: "{{ coremedia_directory.env }}/studio-server/jmx_prometheus.yml"
    #     target: /coremedia/prometheus/jmx_prometheus.yml
    #     type: bind
    #     read_only: true
    #     source_handling:
    #       create: false
    #       owner: "1000"
    #       group: "1000"
    #       mode: "0750"
    #   - source: "{{ coremedia_directory.heapdumps }}/studio-server"
    #     target: /coremedia/heapdumps
    #     type: bind
    #     source_handling:
    #       create: true
    #       owner: "1000"
    #       group: "1000"
    #       mode: "0770"
    #   - source: "{{ coremedia_directory.cache }}/studio-server"
    #     target: /coremedia/cache
    #     type: bind
    #     source_handling:
    #       create: true
    #       owner: "1000"
    #       group: "1000"
    #       mode: "0770"
    #   - source: "{{ coremedia_directory.env }}/studio-server/state"
    #     target: /coremedia/state
    #     type: bind
    #     source_handling:
    #       create: true
    #       owner: "1000"
    #       group: "1000"
    #       mode: "0770"
    environments:
      # JAVA_HEAP: 4096m
      # WAIT_HOSTS: "solr-primary-cms.cm.local:40080,workflow-server.cm.local:40380"
      EDITORIAL_COMMENTS_DATASOURCE_URL: "{{ coremedia_dba.studio_editorial }}"
      EDITORIAL_COMMENTS_DB_PASSWORD: "{{ vault__database.studio_editorial }}"
      EDITORIAL_COMMENTS_DB_SCHEMA: "{{ coremedia_dba_user.studio_editorial }}"
      EDITORIAL_COMMENTS_DB_USERNAME: "{{ coremedia_dba_user.studio_editorial }}"
      ELASTIC_SOLR_URL: "{{ coremedia_solr.primary_cms }}"
      ENVIRONMENT_FQDN: studio.cm.local
      MANAGEMENT_ENDPOINTS_WEB_EXPOSURE_INCLUDE: "*"
      MANAGEMENT_HEALTH_PROBES_ENABLED: true
      MONGODB_CLIENTURI: "{{ coremedia_mongodb.cms.url }}"
      REPOSITORY_URL: "{{ coremedia_ior.cms }}"
      SERVER_USE-FORWARD-HEADERS: "true"
      SOLR_COLLECTION_CAE: preview
      SOLR_COLLECTION_CONTENT: studio
      SOLR_URL: "{{ coremedia_solr.primary_cms }}"
      SPRING_BOOT_EXPLODED_APP: "true"
      STUDIO_PREVIEWURLPREFIX: "{{ coremedia_urls.preview }}/blueprint/servlet"
      STUDIO_PREVIEWURLWHITELIST: '*'
      WFS_IOR_URL: "{{ coremedia_ior.wfs }}"
    # properties:
    #   server.forward-headers-strategy: framework
    #   server.tomcat.remoteip.protocol-header: X-Forwarded-Proto
    command:
      - application

  - name: studio-client
    <<: *CONTAINER_DEFAULTS
    labels:
      environment: dev
      watchdog: "true"
      service-discover: "false"
    healthcheck:
      test:
        - "CMD"
        - "curl"
        - "-Lf"
        - "http://localhost:80"
      interval: 30s
      timeout: 10s
      retries: 3
      start_period: 40s
    image: "{{ container_registry_dev }}/coremedia/studio-client:{{ container_tags.coremedia }}"
    memory: 384M
    memory_swap: 384M
    memory_reservation: 384M
    published_ports:
      - 8080:80
    environments:
      PROTOCOL: ""
      MANAGEMENT_ENDPOINTS_WEB_EXPOSURE_INCLUDE: "*"

  - name: cae-preview
    <<: *CONTAINER_DEFAULTS
    <<: *CONTAINER_DEFAULT_LABELS
    <<: *CONTAINER_DEFAULT_HEALTH
    image: "{{ container_registry_dev }}/coremedia/cae-preview:{{ container_tags.coremedia }}"
    memory: 1537m
    memory_swap: 1537m
    memory_reservation: 1537m
    cpus: "1.0"
    published_ports:
      - 40980:8080
      - 40981:8081
      - 40999:8199  # prometheus_exportercae_preview
    mounts: "{{ 'cae-preview' | bodsch.coremedia.container_mounts(
            coremedia_directory,
            need_mounts=['prometheus', 'cache', 'blobcache', 'events_sitemap', 'heapdumps'] + coremedia_container_properties.cae_preview
        ) }}"
    # mounts:
    #   - source: "{{ coremedia_directory.env }}/cae-preview/jmx_prometheus.yml"
    #     target: /coremedia/prometheus/jmx_prometheus.yml
    #     type: bind
    #     read_only: true
    #     source_handling:
    #       create: false
    #       owner: "1000"
    #       group: "1000"
    #       mode: "0750"
    #   - source: "{{ coremedia_directory.heapdumps }}/cae-preview"
    #     target: /coremedia/heapdumps
    #     type: bind
    #     source_handling:
    #       create: true
    #       owner: "1000"
    #       group: "1000"cae_preview
    #       mode: "0770"
    #   - source: "{{ coremedia_directory.cache }}/cae-preview"
    #     target: /coremedia/cache
    #     type: bind
    #     source_handling:
    #       create: true
    #       owner: "1000"
    #       group: "1000"
    #       mode: "0770"
    #   - source: "{{ coremedia_directory.blobcache }}/cae-preview"
    #     target: /coremedia/blobcache
    #     type: bind
    #     source_handling:
    #       create: true
    #       owner: "1000"
    #       group: "1000"
    #       mode: "0770"
    property_files:
      - name: logging.properties
        properties:
          logging.level.cap: off
          logging.level.cap.server: info
          logging.level.com.coremedia: warn
          logging.level.com.coremedia.publisher: info
          logging.level.hox: off
          logging.level.org.apache: off
          logging.level.root: off
    environments:
      # JAVA_HEAP: 5120m
      APPLICATION_OPTS: >-
        -Dspring.config.location={{ '' | bodsch.coremedia.spring_configs(coremedia_container_properties.cae_preview) }}
      # WAIT_HOSTS: mongodb:27017,solr-primary-cms.cm.local:40080,content-management-server.cm.local:40180
      CAE_CORS_ALLOWED_ORIGINS_RESOURCES: "{{ coremedia_urls.preview }}"
      CAE_HASHING_SECRET: "{{ vault__cae.hashing_secret.preview }}"
      CAE_VIEW_DEBUGENABLED: true
      DELIVERY_STANDALONE: true
      DELIVERY_PREVIEW_MODE: true
      CAE_COOKIE_FORCESECURE: false
      CAE_COOKIE_SAMESITE: Lax
      # CAE_MEDIA_CDN_DISABLED: true
      CAE_PREVIEW_CROSSDOMAIN_WHITELIST: "{{ coremedia_urls.studio }},{{ coremedia_urls.preview }},https://player.vimeo.com"
      CAE_PREVIEW_PBE_STUDIO_URL_WHITELIST: "{{ coremedia_urls.studio }}/"
      CORE_REDIRECTS_CACHE_PARALLEL_SITE_RECOMPUTE_THREADS: 1
      ELASTIC_SOLR_URL: "{{ coremedia_solr.primary_cms }}"
      ENVIRONMENT_FQDN: studio.cm.local
      LIVECONTEXT_CROSSDOMAIN_WHITELIST: "*"
      MANAGEMENT_ENDPOINTS_WEB_EXPOSURE_INCLUDE: "*"
      MANAGEMENT_HEALTH_PROBES_ENABLED: true
      MONGODB_CLIENTURI: "{{ coremedia_mongodb.cms.url }}"
      REPOSITORY_URL: "{{ coremedia_ior.cms }}"
      # Logging
      # LOGGING_LEVEL_CAP: info
      # LOGGING_LEVEL_COM_COREMEDIA: info
      # LOGGING_LEVEL_HOX: info
      # LOGGING_LEVEL_ORG_APACHE_CATALINA: warn
      # LOGGING_LEVEL_ORG_APACHE_PDFBOX_PDMODEL_FONT: off
      # LOGGING_LEVEL_ORG_APACHE_TOMCAT: warn
      # LOGGING_LEVEL_ORG_SPRINGFRAMEWORK_BOOT: info
      # LOGGING_LEVEL_ROOT: warn
      # ------------------------------------------------
      # SECURITY_ENABLE_CSRF: "false"
      SOLR_COLLECTION_CAE: preview
      SOLR_URL: "{{ coremedia_solr.primary_cms }}"
      SPRING_APPLICATION_NAME: 'cae-preview'
      SPRING_BOOT_EXPLODED_APP: "true"
      # SPRING_PROFILES: tallence-social-depl
      SECURITY_CSRF_PREVENTION_ENABLED: "false"
      TEST_MODE: "false"
      SOLR_CAE_COLLECTION: "preview"
    command:
      - application
