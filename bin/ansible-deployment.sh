#!/usr/bin/env bash

SCRIPTNAME=$(basename $0 .sh)
VERSION="1.0.0"
VDATE="11.11.2022"

VAULT_FILE=""

# ----------------------------------------------------------------------------------------

version() {

  help_format_title="%-9s %s\n"

  echo ""
  printf  "$help_format_title" "ansible deployment helper"
  echo ""
  printf  "$help_format_title" " Version $VERSION ($VDATE)"
  echo ""
}

usage() {
  help_format_title="%-9s %s\n"
  help_format_desc="%-9s %-30s %s\n"
  help_format_example="%-9s %-30s %s\n"

  version

  printf  "$help_format_title" "Usage:" "$SCRIPTNAME [-h] [-v] ... "
  printf  "$help_format_desc"  ""       "-h | --help"                ": Show this help"
  printf  "$help_format_desc"  ""       "-v | --version"             ": Prints out the Version"
  printf  "$help_format_desc"  ""       "-E | --environment | --env" ": lowercase Environment (valid are: stage, prod, ops, monitoring)"
  printf  "$help_format_desc"  ""       "-P | --playbook"            ": playbook"
  printf  "$help_format_desc"  ""       "-C | --check"               ": enable check mode (dry run)"
  printf  "$help_format_desc"  ""       "-D | --diff"                ": lowercase Environment (valid are: dev, qa, prod)"
  printf  "$help_format_desc"  ""       "-L | --limit"               ": limit to host or group"
  printf  "$help_format_desc"  ""       "-T | --tags"                ": use ansible tag (e.g. coremedia_docker)"
}

# ----------------------------------------------------------------------------------------

environment() {

  if [ -z "${ENVIRONMENT}" ]
  then
    echo "missing environment"
    exit 1
  fi

  # envs=$(find inventories/ -maxdepth 1  -type d -exec basename {} \; | grep -v inventories)

  if [ "${ENVIRONMENT}" = "monitoring" ] || [ "${ENVIRONMENT}" = "coremedia" ] || [ "${ENVIRONMENT}" = "ops" ] || [ "${ENVIRONMENT}" = "dev" ]
  then
    VAULT_FILE="vault/ansible-${ENVIRONMENT}-vault.pass"
  else
    echo "wrong environment"
    exit 1
  fi
}

# ----------------------------------------------------------------------------------------

ansible_vault() {

  if [ -e "${VAULT_FILE}" ]
  then
    export ANSIBLE_VAULT_PASSWORD_FILE="${VAULT_FILE}"
  fi
}

ansible() {

  local inventory="inventories/${ENVIRONMENT}/inventory.yml"

  if [ ! -n "${inventory}" ]
  then
    echo "missing inventory '${inventory}'"
    exit 1
  fi

  if [ ! -n "playbooks/${PLAYBOOK}" ]
  then
    echo "missing playbook: 'playbooks/${PLAYBOOK}'"
    exit 1
  fi

  OPTS="" # -vvvv"
  [ -n "${VERBOSE}" ] && OPTS="${OPTS} --verbose" # --verbose --verbose"
  [ "${CHECK}" ] && OPTS="${OPTS} --check"
  [ "${DIFF}" ]  && OPTS="${OPTS} --diff"
  [ -n "${LIMIT}" ] && OPTS="${OPTS} --limit ${LIMIT}"
  [ -n "${TAGS}" ]  && OPTS="${OPTS} --tags ${TAGS}"

  OPTS="${OPTS} -e 'ansible_ssh_timeout=10'"

  ansible-playbook \
    --inventory ${inventory} \
    playbooks/${PLAYBOOK} \
    ${OPTS}
}

run() {
  environment
  ansible_vault
  ansible
}

# ----------------------------------------------------------------------------------------

# Parse parameters
while [ $# -gt 0 ]
do
  case "${1}" in
    -h|--help)                      usage;          exit 0;   ;;
    -v|--version)                   version;        exit 0;   ;;
    -E|--environment|--env) shift;  ENVIRONMENT="${1}";       ;;
    -P|--playbook)          shift;  PLAYBOOK="${1}";          ;;
    -C|--check)                     CHECK=true;               ;;
    -D|--diff)                      DIFF=true;                ;;
    -L|--limit)             shift;  LIMIT="${1}";             ;;
    -T|--tags)              shift;  TAGS="${1}";              ;;
    -V|--verbose)                   VERBOSE=true;             ;;
    *)
      echo "Unknown argument: '${1}'"
      exit $STATE_UNKNOWN
      ;;
  esac
shift
done

run
