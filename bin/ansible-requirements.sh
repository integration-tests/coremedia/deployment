#!/usr/bin/env bash

set -x

exit_hook() {
  echo "clean up"
  echo "return code: $?"
  # exit 0
}
trap exit_hook INT TERM QUIT EXIT

ANSIBLE_REQUIREMENTS="${ANSIBLE_REQUIREMENTS:-}"

save_requirements() {
if [ "${ANSIBLE_REQUIREMENTS}" != "none" ]
then
  if [ -f "requirements/${ANSIBLE_REQUIREMENTS}" ]
  then
   mv \
    requirements/${ANSIBLE_REQUIREMENTS} \
    requirements/${ANSIBLE_REQUIREMENTS}.SAVE
  fi
fi
}

restore_requirements() {
if [ "${ANSIBLE_REQUIREMENTS}" != "none" ]
then
  if [ -f "requirements/${ANSIBLE_REQUIREMENTS}.SAVE" ]
  then
    mv \
      requirements/${ANSIBLE_REQUIREMENTS}.SAVE \
      requirements/${ANSIBLE_REQUIREMENTS}
  fi
fi
}


if [ -f requirements/collections.yml ]
then
  echo "install ansible collections from 'requirements/collections.yml'"
  ansible-galaxy \
    collection \
    install \
    --requirements-file requirements/collections.yml
fi

save_requirements

if [ "${ANSIBLE_REQUIREMENTS}" != "none" ]
then
  if [ -f "requirements/${ANSIBLE_REQUIREMENTS}" ]
  then
      requirement="requirements/${ANSIBLE_REQUIREMENTS}"
  else
      requirement="requirements/default.yml"
  fi
  echo "install ansible roles from '${requirement}'"

  ansible-galaxy \
      role \
      install \
      --role-file ${requirement}
fi

restore_requirements
