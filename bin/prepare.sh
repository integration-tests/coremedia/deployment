#!/usr/bin/env bash

PIP=$(command -v pip)

if [ -f "python-requirements.txt" ]
then
  echo "install python requirements"
  ${PIP} \
    install \
    --break-system-packages \
    --requirement python-requirements.txt > /dev/null
fi
