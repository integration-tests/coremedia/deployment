#!/usr/bin/env bash

set +e
set +x

ssh_agent=$(pgrep --count --euid jenkins --newest ssh-agent --list-full)

if [ ${ssh_agent} -eq 0 ]
then
  # run ssh-agent with no timeout!
  echo "start new ssh-agent"
  eval $(ssh-agent -s)
else

  # re-use agent
  echo "re-use ssh-agent"
  export SSH_AGENT_PID=$(pgrep --euid jenkins --newest ssh-agent)
  export SSH_AUTH_SOCK=$(ls /tmp/ssh-*/agent.$((${SSH_AGENT_PID}-1)))
fi

if [ -f ~/.ssh_jenkins ]
then
  ssh-add ~/.ssh_jenkins
fi

if [ -f ${HOME}/.config/ssh_jenkins ]
then
    if [ $(ssh-add -l | grep -c ED25519) -eq 0 ]
    then
      echo "add ssh-key ..."
      ssh-add ${HOME}/.config/ssh_jenkins
    fi
fi

ssh-add -l
# set -e
# ssh-add -l
# env | grep SSH
