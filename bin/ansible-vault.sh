#!/usr/bin/env bash

SCRIPTNAME=$(basename $0 .sh)
VERSION="1.0.0"
VDATE="11.11.2022"
VAULT_FILE=""

# ----------------------------------------------------------------------------------------

version() {

  help_format_title="%-9s %s\n"

  echo ""
  printf  "$help_format_title" "ansible vault helper"
  echo ""
  printf  "$help_format_title" " Version $VERSION ($VDATE)"
  echo ""
}

usage() {
  help_format_title="%-9s %s\n"
  help_format_desc="%-9s %-30s %s\n"
  help_format_example="%-9s %-30s %s\n"

  version

  printf  "$help_format_title" "Usage:" "$SCRIPTNAME [-h] [-v] ... "
  printf  "$help_format_desc"  ""       "-h | --help"                ": Show this help"
  printf  "$help_format_desc"  ""       "-v | --version"             ": Prints out the Version"
  printf  "$help_format_desc"  ""       "-E | --environment | --env" ": lowercase Environment (valid are: stage, prod, ops, monitoring)"
}

# ----------------------------------------------------------------------------------------

environment() {

  if [ -z "${ENVIRONMENT}" ]
  then
    echo "missing environment"
    exit 1
  fi

  if [ "${ENVIRONMENT}" = "monitoring" ] || [ "${ENVIRONMENT}" = "coremedia" ] || [ "${ENVIRONMENT}" = "ops" ] || [ "${ENVIRONMENT}" = "dev" ]
  then
    VAULT_FILE="vault/ansible-${ENVIRONMENT}-vault.pass"
  else
    echo "wrong environment"
    exit 1
  fi
}

# ----------------------------------------------------------------------------------------

ansible_vault() {

  if [ -e "${VAULT_FILE}" ]
  then
    export ANSIBLE_VAULT_PASSWORD_FILE="${VAULT_FILE}"
  fi
}

ansible_vault_edit() {

  local vault_file="inventories/${ENVIRONMENT}/group_vars/all/vault"

  ansible-vault \
    edit \
    ${vault_file}

}

run() {

  environment
  ansible_vault
  ansible_vault_edit
}

# ----------------------------------------------------------------------------------------

# Parse parameters
while [ $# -gt 0 ]
do
  case "${1}" in
    -h|--help)                      usage;          exit 0;   ;;
    -v|--version)                   version;        exit 0;   ;;
    -E|--environment|--env) shift;  ENVIRONMENT="${1}";       ;;
    *)
      echo "Unknown argument: '${1}'"
      exit $STATE_UNKNOWN
      ;;
  esac
shift
done

run
